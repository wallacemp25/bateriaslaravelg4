@extends("templates.base")

@section("conteudo")
    <main>
      <h1>Turma: 2D2 - Grupo 1</h1>
      <h2>Participantes:</h2>
      <hr />

      <table class="table table-striped table-bordered">
        <tr>
          <th>Matrícula</th>
          <th>Nome</th>
          <th>Função</th>
        </tr>
        <tr>
          <td>123456</td>
          <td>José da Silva</td>
          <td>Gerente</td>
        </tr>
        <tr>
          <td>123457</td>
          <td>Maria da Silva</td>
          <td>Desenvolvedor HTML e CSS</td>
        </tr>
        <tr>
          <td>123458</td>
          <td>João da Silva</td>
          <td>Medições e deselvolvedor javascript</td>
        </tr>
      </table>
      <img
        class="grupo"
        src="imgs/estudantes.jpeg"
        alt="Componentes do grupo"
      />
    </main>
    @endsection

@section("rodape")
    <h2>rodape da pagina principal</h2>
    <hr>
@endsection
